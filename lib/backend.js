/**
 Backend Interface.
 
 Implement this API for providing a backend for the acl module.
 */

var contract = require('./contract');

var Backend = {
    /**  
     Begins a transaction.
     */
    begin: function () {
        // returns a transaction object
    },
    /**
     Ends a transaction (and executes it)
     */
    end: function (transaction, cb) {
        contract(arguments).params('object', 'function').end();
        // Execute transaction
    },
    /**
     Cleans the whole storage.
     */
    clean: function (cb) {
        contract(arguments).params('function').end();
    },
    /**
     List bucket data by bucket name and meta values.
     */
    list: function (bucket, meta, cb) {
        contract(arguments)
                .params('string', 'object', 'function')
                .end();
    },
    /**
     Sets meta data to Role.
     */
    set: function (bucket, key, update, meta, cb) {
        contract(arguments)
                .params('string', 'string', 'object', 'object', 'function')
                .end();
    },
    /**
     Gets the contents at the bucket's key.
     */
    get: function (bucket, key, meta, cb) {
        contract(arguments)
                .params('string', 'string|number', 'object', 'function')
                .params('string', 'string|number', 'function')
                .end();
    },
    /**
     Returns the union of the values in the given keys.
     */
    union: function (bucket, keys, meta, cb) {
        contract(arguments)
                .params('string', 'array', 'object', 'function')
                .params('string', 'array', 'function')
                .end();
    },
    /**
     Create new empty document by model in selected bucket.
    */
    create: function (transaction, bucket, data) {
        contract(arguments)
                .params('object', 'string', 'object')
                .end();
    },
    /**
     Adds values to a given key inside a bucket.
     */
    add: function (transaction, bucket, key, values, meta) {
        contract(arguments)
                .params('object', 'string', 'string|number', 'string|array|number', 'object')
                .end();
    },
    /**
     Delete the given key(s) at the bucket
     */
    del: function (transaction, bucket, keys, meta) {
        contract(arguments)
                .params('object', 'string', 'string|array', 'object')
                .end();
    },
    /**
     Removes values from a given key inside a bucket.
     */
    remove: function (transaction, bucket, key, values, meta) {
        contract(arguments)
                .params('object', 'string', 'string|number', 'string|array|number', 'object')
                .end();
    }
}

exports = module.exports = Backend;
